.PHONY: all export publish clean

all: export publish

export:
	emacs tutorial.org --batch -l ~/.emacs --eval="(setq org-export-babel-evaluate nil)" -f org-reveal-export-to-html --kill
	emacs tutorial.org --batch -l ~/.emacs --eval="(setq org-export-babel-evaluate nil)" -f org-latex-export-to-pdf --kill

publish:
	# rsync -uva tutorial.html reveal.js ~/Downloads/tuto
	ssh karma "mkdir -p Web/tutorials/reproducible_research"
	rsync -uvaR tutorial.html karma:Web/tutorials/reproducible_research/index.html
	rsync -uvaR tutorial.pdf reveal.js karma:Web/tutorials/reproducible_research/

clean:
	rm -rf auto *.out *.log *.aux *.toc myproject/ env/ 
